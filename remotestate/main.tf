terraform {

  backend "s3" {
    bucket         = "remotestate-6lz52dbb"
    profile        = "festo" 
    key            = "state/terraform.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    kms_key_id     = "alias/terraform-bucket-key"
    dynamodb_table = "terraform-state"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }

}

provider "aws" {
  region  = "eu-central-1"
  profile = "festo"
}
