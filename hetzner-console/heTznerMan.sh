#!/bin/bash

echo "starting Hetzner console-container script"

read -p "Please enter container name:" container_name
read -p "Please enter your Hetzner token:" token

# Build image

podman build --build-arg TOKEN=$token -t $container_name -f Dockerfile.debian

#podman build -t $container_name -f Dockerfile.debian

#Creating toolbox container
toolbox create -i localhost/$container_name:latest

echo "toolbox container finished"
